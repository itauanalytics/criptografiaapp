﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testeCripto
{
    class Program
    {
        static void Main(string[] args)
        {
            String valor = "olá mundo 393939393928";
            String valorCriptografado = CriptografiaApp.Criptografia.Encrypt(valor);
            String valorDescriptografado = DeCriptografiaApp.Decriptografia.Decrypt(valorCriptografado);

            Console.WriteLine(valor);                   //Novo valor
            Console.WriteLine(valorCriptografado);      //jM3IT3OZEy+1ha5XNL3Wfg==
            Console.WriteLine(valorDescriptografado);   //Novo valor
            Console.ReadLine();
        }
    }
}
