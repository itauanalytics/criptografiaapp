﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TesteCriptografia
{
    class Program
    {
        static void Main(string[] args)
        {
            string opcao = "0";

            while (opcao.Equals("0")){
                Console.WriteLine("Digite um valor para Criptografar");
                string valor = Console.ReadLine();

                String valorCript = CriptografiaApp.Criptografia.Encrypt(valor);

                Console.WriteLine("-----------------");
                Console.WriteLine("Valor Criptografado: " + valorCript);
                Console.WriteLine("-----------------");

                Console.WriteLine("Digite um valor para descriptografar \n ou \n Digite 0 para descriptografar o valor anterior");
                string valorInputDescript = Console.ReadLine();

                if ("0".Equals(valorInputDescript))
                {
                    valorInputDescript = valorCript;
                }

                string valorDecript = DeCriptografiaApp.Decriptografia.Decrypt(valorInputDescript);

                Console.WriteLine("-----------------");
                Console.WriteLine("Valor Descriptografado: " + valorDecript);
                Console.WriteLine("-----------------");

                Console.WriteLine("Aperte 0 para reiniciar ou qualquer tecla para sair");
                opcao = Console.ReadLine();
                Console.Clear();
            }
        }
    }
}
